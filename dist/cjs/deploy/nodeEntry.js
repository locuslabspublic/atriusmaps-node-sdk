'use strict';

var e = require('node:http');
var t = require('node:https');
var httpsProxyAgent = require('https-proxy-agent');
var o = require('node-fetch');
var package_json = require('../package.json.js');
var controller = require('../src/controller.js');
var observable = require('../src/utils/observable.js');
var prepareSDKConfig = require('./prepareSDKConfig.js');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var e__default = /*#__PURE__*/_interopDefaultLegacy(e);
var t__default = /*#__PURE__*/_interopDefaultLegacy(t);
var o__default = /*#__PURE__*/_interopDefaultLegacy(o);

const a=package_json["default"].version;let g=!1;const m=e=>e.toString().length<2?"0"+e:e,p=()=>`AtriusMaps Node SDK (${(()=>{const e=new Date;return `${e.getHours()}:${m(e.getMinutes())}:${m(e.getSeconds())}.${e.getMilliseconds()}`})()}): `,l=function(){if(g){let e=p()+Array.from(arguments).map((e=>"object"==typeof e?JSON.stringify(e):e)).join(" ");const t=e.split("\n");t.length>1?e=t[0]+`… (+ ${t.length-1} lines)`:e.length>256&&(e=e.substring(0,255)+`… (length: ${e.length} chars)`),console.log(e);}};async function f(r){return new Promise(((a,g)=>{r.headless=!0,function(r){const s=new e__default["default"].Agent({keepAlive:!0}),c=new t__default["default"].Agent({keepAlive:!0}),i=r.proxy?new httpsProxyAgent.HttpsProxyAgent(`http://${r.proxy.host}:${r.proxy.port}`):null,a=r.agent||i||(e=>"http:"===e.protocol?s:c);global.fetch=async(e,t)=>o__default["default"](e,{...t,agent:a});}(r);const m=prepareSDKConfig(r);controller.create(m).then((e=>{const t=(t,n)=>("string"==typeof t&&(t={...n,command:t}),l("Sending command object: ",t),e.bus.get("clientAPI/execute",t).then((e=>(l("Received Message: ",e),e))).catch((e=>{throw l("Error: ",e.message),e})));observable(t),e.eventListener.observe((function(){t.fire.apply(t,arguments);})),t.on("ready",((e,n)=>{const{commands:o}=n.commandJSON;!function(e,t){t.forEach((t=>{e[t.command]=function(){const n={command:t.command};for(let e=0;e<arguments.length;e++)n[t.args[e].name]=arguments[e];return e(n)};}));}(t,o),a(t),l("map ready");}));}));}))}const h={getVersion:()=>a,newMap:e=>f(e),setLogging:e=>{g=e,e&&l(`Atrius Maps JS SDK Client v${a} Logging enabled.`);}};

module.exports = h;
