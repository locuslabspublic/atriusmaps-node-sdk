'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var e = require('ramda');
var bounds = require('../../../src/utils/bounds.js');
var configUtils = require('../../../src/utils/configUtils.js');

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

var e__namespace = /*#__PURE__*/_interopNamespace(e);

const n=async(t,e)=>t?fetch(e,{headers:{Authorization:t}}):fetch(e),s=t=>e=>n(t,e).then((t=>t.json())),o=t=>e=>n(t,e).then((t=>t.text())),l=t=>`https://api.content.locuslabs.com/${t}`,r=(e,a,n,s,o)=>e__namespace.mapObjIndexed(((t,a)=>((t,e,a,n,s,o)=>"theme"===o||"style"===o?`${l(a)}/${e}/${o}/${s}/${n}/${o}.json`:t.replace(/https:\/\/content.locuslabs.com/gi,l(a)))(t,e,n,s,o,a)),a),i=async(t,e)=>{const n={alpha:"alpha-a.locuslabs.com",beta:"beta-a.locuslabs.com",gamma:"gamma-a.locuslabs.com",prod:"a.locuslabs.com"},{assetStage:s,venueId:o,accountId:l,formatVersion:i}=t,c=`https://${n[s]||n.prod}/accounts/${l}`,u=i||"v5",d=t.dataFetch&&configUtils.global[t.dataFetch]&&configUtils.global[t.dataFetch].getFiles?await configUtils.global[t.dataFetch].getFiles(t):await e(`${c}/${u}.json`);if(!d[o])throw Error(`Attempt to access venue ${o} which is not within venue list: ${Object.keys(d)}`);const g=d[o].files,h=(t.dataFetch&&configUtils.global[t.dataFetch]&&configUtils.global[t.dataFetch].getVenueData?await configUtils.global[t.dataFetch].getVenueData(t):await e(g.venueData))[o];h.tileServerAuthInfo&&function(t){const e={defaultOrdinal:0,defaultStructureId:"singleBuilding",formatVersion:"v5",structures:{singleBuilding:{name:"singleBuilding",boundsPolygon:[],defaultLevelId:"singleLevel",id:"singleBuilding",levels:{singleLevel:{boundsPolygon:[],clfloor:0,details:"",id:"singleLevel",name:"singleLevel",ordinal:0}}}},structureOrder:["singleBuilding"]};for(const a in e)t[a]=e[a];}(h),h.venueList=d;const m=(t=>{const e=t.deepLinkProps?t.deepLinkProps.contentStage:null;return "alpha"===e||"beta"===e||"prod"===e?e:null})(t);return h.files=m?r(h.category,g,m,l,o):g,h},c=t=>{const{structureOrder:a,structures:n}=t;return a.map((t=>{const a=n[t],s=bounds.findBoundsOfCoordinates(a.boundsPolygon);return {...a,bounds:s}}))};const u=([t,e,...a])=>[e,t,...a],d=(t,e)=>{return !t||!Array.isArray(t)||t.length<1?t:(a=t[0][0],n=e.ne.lng,s=e.sw.lng,(a>n?a<=s:t=>s)?t:t.map(u));var a,n,s;};

exports.buildStructures = c;
exports.createFetchJson = s;
exports.createFetchText = o;
exports.getVenueDataFromUrls = i;
exports.normalizeCoords = d;
