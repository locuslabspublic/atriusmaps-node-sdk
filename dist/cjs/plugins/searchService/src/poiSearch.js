'use strict';

var e = require('ramda');
var utils = require('./utils.js');

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

var e__namespace = /*#__PURE__*/_interopNamespace(e);

function r(r,e){const o=utils.getFlexSearchInstance({lang:e});function n(a){return Object.values(a).map((a=>{const{poiId:r,category:e="",name:o,keywords:n=[],roomId:i=""}=a,c=e__namespace.path(["dynamicData","grab","tags"],a)||[],s=n.filter(e__namespace.prop("isUserSearchable")).map(e__namespace.prop("name")),p=`${o} ${e.split(".").join(" ")} ${i} ${s.join(" ")} ${c.join(" ")}`;return [Number(r),p]}))}return n(r).forEach((([t,a])=>o.add(t,a))),{search:function(a){const e={...a};e.limit||(e.limit=5e3);const n=o.search(e);return Object.values(e__namespace.pick(n,r))},updateMultiple:function(t){n(t).forEach((([t,a])=>o.update(t,a)));}}}

module.exports = r;
