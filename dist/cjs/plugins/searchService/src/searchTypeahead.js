'use strict';

var e = require('ramda');
var utils = require('./utils.js');

function s(s,d,u){const c=function(s,d){const u=function(e){return Object.values(e).map((e=>e.category)).map((e=>e.split("."))).map((e=>e[0]))}(s),c=e.pipe(e.values,e.chain(e.prop("keywords")),e.filter(e.prop("isUserSearchable")),e.pluck("name"))(s),m=[...u,...c],p=Array.from(new Set([...m])),l=utils.getFlexSearchInstance({lang:d,type:"typeahead"});p.forEach(((e,r)=>l.add(r,e)));return {search:e=>l.search(e).map((e=>p[e])),add:e=>{p.push(e),l.add(p.length-1,e);}}}(s,u);return {query:(e,r)=>{const t=c.search({query:e,limit:r}),a=!(e.length<3)&&t.length,n=r-t.length,o=a?function(e,r){const t=d({query:e,limit:r}),a=d({query:e,suggest:!0,limit:r}),n=t.map((e=>e.poiId)),o=a.filter((e=>-1===n.indexOf(e.poiId)));return t.concat(o)}(e,n):[];return {keywords:t,pois:o}},addKeyword:e=>{c.add(e);}}}

module.exports = s;
