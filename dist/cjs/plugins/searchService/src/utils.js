'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var e = require('flexsearch');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var e__default = /*#__PURE__*/_interopDefaultLegacy(e);

const n=["ko","ja","zh-Hans","zh-Hant"],r=({lang:r,type:t="standard"})=>{const s={tokenize:"forward",rtl:"ar"===r,stemmer:{s:"",es:"",ies:"y"}};return n.includes(r)&&(s.tokenize="full"),new e__default["default"].Index(s)};

exports.getFlexSearchInstance = r;
