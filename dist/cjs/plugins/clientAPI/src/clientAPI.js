'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var index = require('../../../src/extModules/flexapi/src/index.js');

function t(t,n){const s=index();return t.bus.on("clientAPI/registerCommand",(e=>s.registerCommand(e,(e=>t.bus.get(`clientAPI/${e.command}`,e))))),t.bus.on("clientAPI/registerCustomType",(({name:e,spec:t})=>s.registerCustomType(e,t))),t.bus.on("clientAPI/execute",(e=>s.execute(e))),{init:()=>{}}}

exports.create = t;
