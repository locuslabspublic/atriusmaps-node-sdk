'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var e = require('ramda');
var wayfinder = require('./wayfinder.js');

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

var e__namespace = /*#__PURE__*/_interopNamespace(e);

const i=t=>i=>e__namespace.find((e=>e.dst===t),i.edges),r=(r,s,a,n={})=>{const o=[],u=[];let c=!1,l=!1;const d=((e,t,i,r)=>{if(!t||!i)throw Error("bad calculate Route request!");return e.findShortestPath(t,i,r)})(r,s,a,n);if(null===d)return null;let y=null;for(let r=0;r<d.length;r++){const s={distance:0,eta:0};if(y){const e=i(d[r].id)(y);null!=e&&(s.distance=e.distance,s.eta=e.transitTime,"Ground"!==e.type&&(s.portalType=e.type,s.isPortal=!0),e.o&&(s.poiId=e.o),e.path&&(s.curvedPathForward=e.path),e.securityWaitTimes&&(s.securityWaitTimes=e.securityWaitTimes,s.eta=s.securityWaitTimes.queueTime),e.securityLane&&(s.securityLane=e.securityLane,s.isSecurityCheckpoint=!0,e.securityLane.type===wayfinder.SecurityLaneType.SECURITY&&(c=!0),e.securityLane.type===wayfinder.SecurityLaneType.IMMIGRATION&&(l=!0),e.o&&u.push(e.o)));}s.levelDifference=y?d[r].ordinal-y.ordinal:0,s.position=e__namespace.omit(["edges","id"],{...d[r]}),o.push(s),y=d[r];}return e__namespace.last(o).isDestination=!0,{waypoints:o,queues:u,hasSecurity:c,hasImmigration:l}};

exports.findRoute = r;
