'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var e$1 = require('ramda');

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

var e__namespace = /*#__PURE__*/_interopNamespace(e$1);

function e(e,s){const o=Object.values(e),n=function(a,e){const s=[e],o=new Set;for(;s.length;){const e=s.splice(0,1)[0];e&&!o.has(e)&&(o.add(e),e.edges.forEach((e=>{o.has(a[e.dst])||s.push(a[e.dst]);})));}return o}(e,s||o[0]),r=o.filter((a=>!n.has(a)));return console.log(`${r.length} Orphaned nodes found from ${o.length} total`),{orphaned:r,orphanedByFloor:e__namespace.groupBy(e__namespace.prop("floorId"),r),connected:Array.from(n)}}const s=a=>({nodes:n(a),edges:r(a)}),o=e=>e__namespace.map(e__namespace.pipe(e__namespace.assoc("isOrphaned",e),e__namespace.dissoc("edges"))),n=e__namespace.pipe(e,e__namespace.pick(["connected","orphaned"]),e__namespace.evolve({connected:o(!1),orphaned:o(!0)}),e__namespace.values,e__namespace.flatten),r=a=>Object.values(a).flatMap((a=>a.edges)).map((e=>t(e,a))),t=({src:a,dst:e,type:s,isDriveway:o},n)=>({startCoordinates:[n[a].lng,n[a].lat],endCoordinates:[n[e].lng,n[e].lat],isDriveway:o,ordinal:n[a].ordinal,category:d(s),defaultStrokeColor:l(s)}),l=e__namespace.cond([[e__namespace.equals("Stairs"),e__namespace.always("#EFBC9B")],[e__namespace.equals("Elevator"),e__namespace.always("#A491D3")],[e__namespace.equals("Escalator"),e__namespace.always("#563F1B")],[e__namespace.equals("Ramp"),e__namespace.always("#DBD053")],[e__namespace.T,e__namespace.always("#FF0000")]]),d=e__namespace.cond([[e__namespace.equals("Train"),e__namespace.always("nav.train")],[e__namespace.equals("Bus"),e__namespace.always("nav.transit")],[e__namespace.equals("Security Checkpoint"),e__namespace.always("nav.secure")],[e__namespace.equals("Ground"),e__namespace.always("nav.primary")],[e__namespace.T,e__namespace.always("")]]);

exports.enrichDebugNavGraph = s;
exports.orphanTest = e;
