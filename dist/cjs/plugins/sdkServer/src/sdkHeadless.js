'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var e = require('ramda');
var util = require('./util.js');

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

var e__namespace = /*#__PURE__*/_interopNamespace(e);

const s=[{command:"destroy"},{command:"getDirections",args:[{name:"from",type:"location"},{name:"to",type:"location"},{name:"accessible",type:"boolean",optional:!0},{name:"queueTypes",type:"list",itemType:{type:"string"},optional:!0}]},{command:"getDirectionsMultiple",args:[{name:"locations",type:"list",itemType:{type:"location"}},{name:"accessible",type:"boolean",optional:!0},{name:"queueTypes",type:"list",itemType:{type:"string"},optional:!0}]},{command:"getPOIDetails",args:[{name:"poiId",type:"integer",min:0}]},{command:"getAllPOIs"},{command:"getStructures"},{command:"getVenueData"},{command:"search",args:[{name:"term",type:"string",minLength:2},{name:"details",type:"boolean",optional:!0}]}];function i(s){s.bus.on("clientAPI/destroy",(async()=>s.destroy())),s.bus.on("clientAPI/getDirections",(async({from:n,to:i,accessible:a,queueTypes:o})=>{const c=await util.locationToEndpoint(s,n),r=await util.locationToEndpoint(s,i),l={requiresAccessibility:!!a};return o&&(l.selectedSecurityLanes={SecurityLane:o}),s.bus.get("wayfinder/getRoute",{fromEndpoint:c,toEndpoint:r,options:l}).then(e__namespace.pick(["distance","time","steps","navline","waypoints"]))})),s.bus.on("clientAPI/getDirectionsMultiple",(async({locations:n,accessible:i,queueTypes:a})=>{const o=await Promise.all(n.map((async e=>util.locationToEndpoint(s,e)))),c={requiresAccessibility:!!i};a&&(c.selectedSecurityLanes={SecurityLane:a});const r=await Promise.all(e__namespace.aperture(2,o).map((async e=>s.bus.get("wayfinder/getRoute",{fromEndpoint:e[0],toEndpoint:e[1],options:c})))),l=e__namespace.map(e__namespace.pick(["distance","time","steps","navline","waypoints"]),r);return {total:{distance:e__namespace.sum(e__namespace.map((e=>e.distance),l)),time:e__namespace.sum(e__namespace.map((e=>e.time),l))},directions:l}})),s.bus.on("clientAPI/getPOIDetails",(async({poiId:e})=>s.bus.get("poi/getById",{id:e}))),s.bus.on("clientAPI/getAllPOIs",(async()=>s.bus.get("poi/getAll"))),s.bus.on("clientAPI/getStructures",(()=>util.getStructures(s)));const i=e=>"function"!=typeof e;s.bus.on("clientAPI/getVenueData",(async()=>{const t=await s.bus.get("venueData/getVenueData");return e__namespace.filter(i,t)})),s.bus.on("clientAPI/search",(async({term:e,details:t})=>s.bus.get("search/queryAsync",{term:e}).then((n=>{const i=n.map((e=>e.poiId));return s.bus.send("event/search",{referrer:"prog",searchMethod:null,query:e,entities:i}),t?n:i}))));}

exports.handleHeadless = i;
exports.headlessCommands = s;
