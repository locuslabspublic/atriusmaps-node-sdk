'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var geom = require('../../../src/utils/geom.js');

const n=async o=>o.bus.get("venueData/getVenueData").then((o=>o.structures));async function i(i,r){if(r.poiId)return i.bus.get("wayfinder/getNavigationEndpoint",{ep:r.poiId});const{lat:l,lng:a,ord:e,floorId:d,title:u=""}=r,s=await n(i),f=void 0!==e?geom.getFloorAt(s,l,a,e):geom.getFloor(s,d);if(void 0!==e)return {lat:l,lng:a,ordinal:e,floorId:f?f.id:null,title:u};if(!f)throw Error("Call to locationToEndpoint with no ordinal and no floorId (or an invalid one): "+d);return {lat:l,lng:a,floorId:d,ordinal:f.ordinal,title:u}}

exports.getStructures = n;
exports.locationToEndpoint = i;
