'use strict';

var a$1 = require('zousan');
var help = require('./help.js');
var validate = require('./validate.js');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var a__default = /*#__PURE__*/_interopDefaultLegacy(a$1);

const i=/^[-_.0-9a-zA-Z]+$/,s=JSON.stringify;function a(){const a=[],c=()=>a.map((n=>n.sig)),d={},f={customTypes:d,commandDefsList:a};function u(n,o){const r=n.command;if(void 0===r)throw Error(`Invalid command specification in registerCommand: ${s(n)}. No 'command' property specified.`);if(!i.test(r)){throw Error(`Invalid command specification in registerCommand: ${s(n)}. Command name '${r}' not valid.`)}return a.push({sig:n,fn:o}),p}function p(i){return new a__default["default"](((n,a)=>{if(!i)return a(new Error(`No command specified in command object ${s(i)}`));const c=f.commandDefsList.filter((n=>n.sig.command===i.command));if(0===c.length)return a(new Error(`No API command '${i.command}' found.\n${help.getHelpList(f.commandDefsList.map((n=>n.sig)))}`));const d=validate.getSigMatch(f,i);if(!d)return 1===c.length?a(new Error(`Required fields not present in ${s(i)}\n${help.getHelpHeader()}${help.getHelp(c[0].sig)}`)):a(new Error(`Command arguments did not match any required signatures: ${s(i)}\n${help.getHelpList(f.commandDefsList.map((n=>n.sig)))}`));try{validate.validate(f,d.sig,i);}catch(n){return a(n)}n(d.fn(i));}))}return u({command:"help",args:[]},(()=>help.getHelpList(c()))),u({command:"getCommandJSON",args:[]},(()=>({commands:c(),customTypes:d}))),{registerCommand:u,registerCustomType:function(n,o){d[n]=o;},execute:p}}

module.exports = a;
