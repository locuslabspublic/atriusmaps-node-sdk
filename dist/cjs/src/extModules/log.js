'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const e={black:"[30m",red:"[31m",green:"[32m",yellow:"[33m",blue:"[34m",magenta:"[35m",cyan:"[36m",white:"[37m"};function o(n,r){const t=function(o,n={}){let r=o+": ",t=null;if(n.color)if(n.isBrowser)r="%c"+r,t=`color: ${n.color}`;else {const o=e[n.color];o&&(r=o+r);}function l(e){return function(){if(void 0===n.enabled||n.enabled||e===console.error){let o=Array.from(arguments);t&&o.unshift(t),o.unshift(r),n.truncateObjects&&e!==console.error&&(o=o.map((e=>{return "object"==typeof e?(o=JSON.stringify(e),r=parseInt(n.truncateObjects)||100,o&&o.length>r?o.substring(0,r)+"...":o):e;var o,r;})));const l=!n.logFilter||((e,o)=>"string"==typeof o?e[0].includes(o):o.test(e[0]))(o,n.logFilter);(l||e===console.error)&&e.apply(console,o);}}}const c=l(console.log);return c.info=c,c.warn=l(console.warn),c.detailed=l(console.debug),c.error=l(console.error),c.setEnabled=e=>{n.enabled=e;},c}(n,r);return t.sublog=(e,t)=>o(n+"."+e,Object.assign(Object.create(r),t)),t}

exports.initLog = o;
