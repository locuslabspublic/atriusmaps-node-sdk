'use strict';

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

async function t(t,e){const o=await(async()=>{if(!t||!t.type)throw Error("No authentication type found in config");return Promise.resolve((t=>{switch(t.type){case"ms":return "MSAuth";case"cognito":return "CognitoAuth";case"google":return "GoogleAuth";default:throw Error(`Unknown auth type: '${t.type}'`)}})(t)).then((t=>(function (t) { return Promise.resolve().then(function () { return /*#__PURE__*/_interopNamespace(require(t)); }); })(`./${t}`))).then((o=>o.default(t.config,e)))})();return {getToken:async()=>o.getToken(),isLoggedIn:async()=>o.isLoggedIn(),federatedLogin:async()=>o.federatedLogin(),logout:()=>o.logout(),getAccountInfo:()=>o.getAccountInfo(),getType:()=>t.type}}

module.exports = t;
