'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var e = require('IObject');
var app = require('./app.js');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var e__default = /*#__PURE__*/_interopDefaultLegacy(e);

let r=new e__default["default"];const n=new e__default["default"],a=e=>"undefined"!=typeof window?window.alert(e):console.error(e);async function o(o){if(!o)throw Error("Attempt to create App instance with no configuration");let s=new e__default["default"](Object.assign({},n,o));const c=s.appName||"Instance"+(Object.keys(r).length+1);s=s.set("appName",c);try{const e=await app.create(s);return r=r.set(c,e),e}catch(e){console.error(e),e.message?a(e.message):a("Error creating map. Please try again later.");}}

exports.create = o;
