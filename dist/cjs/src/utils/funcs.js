'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

require('ramda');
require('zousan');

let n=null;const t=t=>function(){return n=n?n.then((()=>t.apply(null,arguments))):t.apply(null,arguments),n};function l(n,t){const l={};return Object.keys(t).forEach((o=>{n(o,t[o])&&(l[o]=t[o]);})),l}

exports.filterOb = l;
exports.singleFile = t;
