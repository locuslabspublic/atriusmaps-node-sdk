'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var e = require('ramda');

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

var e__namespace = /*#__PURE__*/_interopNamespace(e);

const r=r=>{const p=e__namespace.mergeAll(r.map(e__namespace.prop("levels"))),l=o=>p[o],e=p=>e__namespace.find(e__namespace.path(["levels",p]),r);return {floorIdToOrdinal:e__namespace.pipe(l,e__namespace.prop("ordinal")),floorIdToFloor:l,floorIdToStructureId:e__namespace.pipe(e,e__namespace.prop("id")),floorIdToStructure:e}};

exports.buildStructuresLookup = r;
