'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const t=(t,e)=>"string"==typeof t?(e||document).querySelector(t):t||null,e=(t,e)=>Array.prototype.slice.call((e||document).querySelectorAll(t));function n(t,e){const n=t||{};e=e?"string"==typeof e?document.querySelector(e):e:document.body;let l="div";n.tag&&(l=n.tag);const s=document.createElement(l);for(const t in n)null!=n[t]&&("klass"===t?s.setAttribute("class",n.klass):"tag"===t||("styles"===t?o(s,n.styles):"text"===t?s.textContent=n.text:"html"===t?s.innerHTML=n.html:s.setAttribute(t,n[t])));return e.appendChild(s),s}const o=(t,e)=>{for(const n in e)t.style[n]=e[n];};

exports.$ = t;
exports.$$ = e;
exports.ad = n;
exports.setStyles = o;
