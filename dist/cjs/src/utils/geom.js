'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var helpers = require('@turf/helpers');
var e$1 = require('@turf/point-to-line-distance');
var e = require('ramda');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

var e__default = /*#__PURE__*/_interopDefaultLegacy(e$1);
var e__namespace = /*#__PURE__*/_interopNamespace(e);

const l=(n,t)=>{const e=n[0],r=n[1];let l=!1;for(let n=0,o=t.length-1;n<t.length;o=n++){const u=t[n][0],s=t[n][1],i=t[o][0],f=t[o][1];s>r!=f>r&&e<(i-u)*(r-s)/(f-s)+u&&(l=!l);}return l},o=n=>{const{n:t,s:e,e:r,w:l}=n.bounds;return [[t,r],[t,l],[e,l],[e,r],[t,r]]};function u(n,t,e,u){if(!e__namespace.length(n))return null;const s=(n=n.filter((n=>null==n.shouldDisplay||!0===n.shouldDisplay))).filter((n=>l([t,e],o(n))));if(0===s.length)return null;if(1===s.length&&!u)return e__namespace.head(s);const i=s.filter((n=>l([t,e],n.boundsPolygon)));if(1===i.length)return e__namespace.head(i);if(i.length)return h(s);if(u)return null;const c=s.map((n=>f(t,e,n))),d=Math.min.apply(null,c);return s[c.indexOf(d)]}const s=n=>helpers.lineString(n.boundsPolygon.map((n=>d(n)))),i={},f=(n,t,e)=>{for(let r=0;r<99;r++)c(n,t,e);return c(n,t,e)},c=(t,r,l)=>{const o=helpers.point([r,t]),u=(n=>{let t=i[n.id];return t||(t=s(n),i[n.id]=t),t})(l);return e__default["default"](o,u)},d=n=>[n[1],n[0]],a=n=>{if(!n.bounds)return 0;const{n:t,s:e,e:r,w:l}=n.bounds;return Math.abs((t-e)*(r-l))},h=n=>{return n.reduce((t=a,(n,e)=>t(n)<t(e)?n:e));var t;},p=(n,t)=>Object.values(n.levels).reduce(((n,e)=>e.ordinal===t?e:n),void 0),g=(n,t)=>n.reduce(((n,e)=>Object.values(e.levels).reduce(((n,e)=>e.id===t?e:n),null)||n),void 0);function m(n,t,e,r,l){const o=u(n,t,e,l);return {building:o,floor:o?p(o,r):null}}const b=(n,t,e,r,l)=>m(n,t,e,r,l).floor;function v(n,t,e,r,l,o,u,s){let i=0,f=0,c=0,d=0,a=0;const h=[{x:n,y:t}];for(let p=1,g=0;p<=20;++p)g=p/20,i=1-g,f=i*i,c=f*i,d=g*g,a=d*g,h.push({x:c*n+3*f*g*e+3*i*d*l+a*u,y:c*t+3*f*g*r+3*i*d*o+a*s});return h}

exports.bezierCurveTo = v;
exports.getBuildingAndFloorAtPoint = m;
exports.getFloor = g;
exports.getFloorAt = b;
exports.getStructureAtPoint = u;
exports.ordToFloor = p;
exports.pointInPolygon = l;
