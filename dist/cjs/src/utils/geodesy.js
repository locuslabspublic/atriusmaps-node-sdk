'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const t=t=>t*Math.PI/180;function a(a,h,n,s){const M=t(a),o=t(n),c=t(n-a),i=t(s-h),r=Math.sin(c/2)*Math.sin(c/2)+Math.cos(M)*Math.cos(o)*Math.sin(i/2)*Math.sin(i/2);return 6371e3*(2*Math.atan2(Math.sqrt(r),Math.sqrt(1-r)))}

exports.distance = a;
exports.toRadians = t;
