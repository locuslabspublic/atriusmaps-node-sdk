'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function n(t,e,o,r){let u=e;o&&(u+="-"+o);const i=t[u];return i||(o?o.indexOf("-")>0?n(t,e,o.substring(0,o.indexOf("-")),r):n(t,e,null,r):r)}function t(n,t){if(!n.config.debug)return !1;let e=n.config.debug[t];return null!=e&&!1!==e&&(!0===e||(e=e.toLowerCase?e.toLowerCase():e,"no"!==e&&"false"!==e))}exports.global = void 0;try{exports.global=Function("return this")();}catch(n){exports.global=window;}

exports.debugIsTrue = t;
exports.getLocalized = n;
