'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var e = require('ramda');

function r(i){return m(i.map(e.path(["position","lat"])).filter(e.identity),i.map(e.path(["position","lng"])).filter(e.identity))}function a(t){return m(t.map(e.prop(0)).filter(e.identity),t.map(e.prop(1)).filter(e.identity))}function m(t,n){return {n:Math.max(...t),s:Math.min(...t),e:Math.max(...n),w:Math.min(...n)}}

exports.findBoundsOfCoordinates = a;
exports.findBoundsOfWaypoints = r;
