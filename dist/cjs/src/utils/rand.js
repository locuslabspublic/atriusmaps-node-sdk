'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const t={nextInt:t=>Math.floor(Math.random()*t)};function n(n,e,o){let s;o=o||t,e||(e=n),n>e&&(n=e);const h=[];if(e/n>=2||e<40)for(;h.length<n;)s=o.nextInt(e),h.indexOf(s)<0&&h.push(s);else {const t=[];for(s=0;s<e;s++)t.push(s);for(s=0;s<n;s++){const n=o.nextInt(t.length);h.push(t[n]),t.splice(n,1);}}return h}const e=(t,e)=>((t,n)=>n.map((n=>t[n])))(t,n(e=e||1,t.length));

exports.arrayPick = e;
exports.randomSet = n;
