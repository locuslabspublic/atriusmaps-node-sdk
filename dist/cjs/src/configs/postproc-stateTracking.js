'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

require('query-string');
var e = require('ramda');
require('zousan');

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

var e__namespace = /*#__PURE__*/_interopNamespace(e);

function r(r,o,t){const n=JSON.parse(o);return Object.keys(n).forEach((o=>{const p=n[o];let s=r.plugins[o];if(!s&&t&&(s=r.plugins[o]={}),s){const r=s.deepLinkProps;s.deepLinkProps=r?e__namespace.mergeDeepRight(r,p):p;}})),r}

exports.setStateFromStateString = r;
