# atriusmaps-node-sdk

## To Install:

`npm install atriusmaps-node-sdk`

Or with yarn:

`yarn add atriusmaps-node-sdk`

Then within your code, import the map initializer via:

`import Init from 'atriusmaps-node-sdk'`

or use `require`

`const Init = require("atriusmaps-node-sdk")`

## To Use

The Init object contains 3 methods:

- `Init.setLogging(boolean logging)` : To turn on/off the logging
- `Init.getVersion()` : Returns the current version of the library
- `Init.newMap(Object configuration)` : This is how  your initialize a new map. This returns a Promise that resolves to your map.

The configuration object recognizes the following properties:

- `accountId`: This is the customer account against which you wish to display a map. Each account is associated with a list of 1 or more venues that it is authorized to display.
- `venueId`: The venue ID you wish the map to render.
- `agent` (optional): An instance of the http.agent to handle network fetches. See https://github.com/node-fetch/node-fetch#custom-agent for more information.
- `proxy` (optional): An object containing a `host` and `port` property to utilize a forwarding proxy for all network requests. (see example below)

At a minimum, a configuration would contain an accountId and a venueId:

```js
const config = {
			venueId: '<venueId>',
			accountId: '<accountId>'
		}
```

An example of utilizing a proxy:

```js
const config = {
			venueId: '<venueId>',
			accountId: '<accountId>',
			proxy: {
				host: 'example.com',
				port: 9108
			}
		}
```

You then initialize your map:

```js
const map = await Init.newMap(config)
```

Your map function is ready to receive commands – of which the following are currently supported:

- `help` : Returns a string indicating all available commands and their arguments
- `getDirections`: Get time, distance and navigation steps from one point to another
- `getPOIDetails`: Get detailed information about a POI by ID
- `getAllPOIs`: Get a list of all POIs for the venue
- `getStructures`: Returns a list of structures (buildings) within the venue along with their properties
- `getVenueData`: Returns a complete venue object containing all venue details
- `search`: Performs a search against a term specified

For details on these commands, including their arguments, return value formats, and examples, see https://locusmapsjs.readme.io/docs/commands

Note that all these commands are asynchronous, and return a promise. So use them with `await` or a `then` clause.

Examples:

```js
const poi = await map.getPOIDetails(11)
console.log(`Got POI details for ${poi.name}.`)
```

Or

```js
map.getPOIDetails(11)
  .then(poi => console.log(`Got POI Details for ${poi.name}.`))
```

For example:

```bash
node main.js
```
